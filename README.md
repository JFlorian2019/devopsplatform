![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg)

---

Example Application from Idea to Execution using Gitlab DevOps platform.

Learn more about GitLab DevOps platform https://about.gitlab.com/handbook/marketing/strategic-marketing/usecase-gtm/devops-platform/ and the official
documentation https://docs.gitlab.com/

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [GitLab VC&C](#gitlab-ci)
- [GitLab CI](#gitlab-user-or-group-pages)
- [Did you fork this project?](#did-you-fork-this-project)
- [Troubleshooting](#troubleshooting)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## GitLab CI

This project's is  built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```
image: alpine:latest

pages:
  stage: deploy
  script:
  - echo 'Nothing to do...'
  artifacts:
    paths:
    - public
  only:
  - master
```

The above example expects two dockerfiles and flask application


